# Projet 3 - Android



## Objectif

Ceci est un projet de cours, fait en binome. L'objectif était de développer une application Android (utilisation d'Android Studio).
Le choix fut fait de partir sur une application regroupant plusieurs mini-jeux afin de pouvoir tester les différentes fonctionnalitées propre à un smartphone.
Une grande partie fut développée en Kotlin (nous avions vu Java en cours mais souhait d'essayer Kotlin).
Durant ce projet, j'ai travaillé majoritairement sur l'activité Distribution de tractes ainsi que la musique et le système de centrale en arrière plan.